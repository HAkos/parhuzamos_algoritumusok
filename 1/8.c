#include <stdio.h>
#include <sys/stat.h>

void fwrite_intarr(char* fname, int* arr, size_t arsz)
{
	FILE* fp = fopen(fname, "w");
	
	for (int i = 0; i < arsz; i++)
	{
		fprintf(fp, "%d\n", arr[i]);
	}
	
	fclose(fp);
}

void fwrite_floatarr(char* fname, float* arr, size_t arsz)
{
	FILE* fp = fopen(fname, "w");
	
	for (int i = 0; i < arsz; i++)
	{
		fprintf(fp, "%f\n", arr[i]);
	}
	
	fclose(fp);
}

void fwrite_longarr(char* fname, long* arr, size_t arsz)
{
	FILE* fp = fopen(fname, "w");
	
	for (int i = 0; i < arsz; i++)
	{
		fprintf(fp, "%d\n", arr[i]);
	}
	
	fclose(fp);
}

int* fread_intarr(char* fname)
{
	FILE* fp = fopen(fname, "r");
	
	fclose(fp);
}

float* fread_floatarr(char* fname)
{
	FILE* fp = fopen(fname, "r");
	
	fclose(fp);
}

long* fread_longarr(char* fname)
{
	FILE* fp = fopen(fname, "r");
	
	fclose(fp);
}

off_t getfsz(const char* path)
{
	struct stat s;
	stat(path, &s);
	
	return s.st_size;
}

int main()
{

	return 0;
}
