#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>

int main()
{
	srand(time(NULL));
	
	time_t start, end;
	
	int a = rand() % 1000;
	int b = rand() % 1000;
	
	printf("%d + %d\n", a, b);
	
	bool guessed = false;
	time(&start);
	do {
		int guess;
		if (scanf("%d",&guess) != 1) {
			printf("Input the sum of the two numbers!\n");
			while (getchar() != '\n');
		} 
		else {
			if (guess == a + b) 
				guessed = true;
			else
				printf("Incorrect guess\n");
		}
	} while (!guessed);
	
	time(&end);
	
	printf("You guessed the number in %d seconds\n", end - start);
	
	return 0;
}
