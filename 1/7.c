#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <stdbool.h>

/*
	Grafikon elkészíthető a program lefutattása után
	a 7_primecountergraph.py fájllal
*/
bool is_prime(int n)
{
	if (n <= 1)
		return false;
	for (int i = 2; i <= sqrt(n); i++) {
		if (n % i == 0) {
			return false;
		    break;
		}
	}
	
	return true;
}

float calc_elapsed_ms(struct timespec *start, struct timespec *end)
{
  return (float) (end->tv_nsec - start->tv_nsec) / 1000000;
}

int main()
{	
	FILE* fp = fopen("7_time", "w");
	struct timespec start;
	struct timespec end;
	
	clock_gettime(CLOCK_MONOTONIC_RAW, &start);
	for (int i = 1000; i <= 20000; i+=1000) {
		int primec = 0;
		
		for (int j = 1; j <= i; j++) {
			if (is_prime(j))
				primec++;
		}
		
		printf("%d, primes: %d\n", i, primec);
	}
    
	clock_gettime(CLOCK_MONOTONIC_RAW, &end);
	float time = calc_elapsed_ms(&start, &end);
	printf(" elapsed time: %f ms\n", time);
	return 0;
}
