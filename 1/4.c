#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int irand_range(int min, int max)
{
	return rand() % (max - min + 1) + min;
}

float frand_range(float min, float max)
{
	float random = ((float) rand()) / (float) RAND_MAX;
    return min + random * (max - min);
}

int main()
{
	srand(time(NULL));
	
	printf("Random int: %d\n", irand_range(500, 1000));
	printf("Random float: %f\n", frand_range(500, 1000));
	
	return 0;
}
