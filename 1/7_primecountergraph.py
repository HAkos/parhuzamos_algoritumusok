import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
lines = []

with open("7_time", "r") as f:
	lines = f.read().splitlines()
	
print(lines)

x = [l.split(',')[0] for l in lines]
y = [l.split(',')[1] for l in lines]

print(x)
print(y)

plt.plot(x, y)

plt.xticks(rotation = 35)
plt.xlabel('Vizsgált intervallum (1..x)')
plt.ylabel('Futási idő (ms)')

plt.tight_layout()
plt.savefig('7_runtime.png')
plt.show()

