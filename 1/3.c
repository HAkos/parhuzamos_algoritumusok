#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

int main(int argc, char** argv)
{
	printf("Sleeping for %d seconds\n", atoi(argv[1]));
	sleep(atoi(argv[1]));
	return 0;
}
