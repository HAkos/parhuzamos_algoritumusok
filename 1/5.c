#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int irand_range(int min, int max)
{
	return rand() % (max - min + 1) + min;
}

int main(int argc, char** argv)
{
	if (argc != 3) {
		printf("Incorrect arguments!\n");
		exit(1);
	}
	
	int min = atoi(argv[1]);
	int max = atoi(argv[2]);
	
	printf("Random number between %d and %d: %d\n",
			min, max, irand_range(min, max));
	
	return 0;
}
