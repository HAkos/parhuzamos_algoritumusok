//	Készítette: Horváth Ákos

#include <stdio.h>

int main()
{
	int a = 123;
	// 8 karakter hosszu kiiras, szokozokkel
	printf("%8d\n", a);
	// 0-val
	printf("%08d\n", a);
	
	return 0;
}
