typedef struct {
    int capacity;
    int size;
    int *data;
} Array;

void array_init(Array* arr, int capacity);
int array_append(Array* arr, int el);
int array_remove(Array* arr, int i);


