#include <stdio.h>
#include <stdlib.h>
#include "array.h"


int main(void)
{
    Array arr;
    array_init(&arr, 20);
    for(int i = 0; i < arr.capacity; i++) {
        array_append(&arr, i*2);
        printf("array index: %d value: %d\n", i, arr.data[i]);
    }

    array_remove(&arr, 4);
    for(int i = 0; i < arr.size; i++) {
        printf("array index: %d value: %d\n", i, arr.data[i]);
    }

    return 0;
}


void array_init(Array* arr, int capacity) {
    arr->data = malloc(capacity*sizeof(int));
    arr->capacity = capacity;
    arr->size = 0;
}

int array_append(Array* arr, int el)
{
    if (arr->size+1 > arr->capacity)
        return -1;

    arr->size++; 
    arr->data[arr->size-1] = el;
    
    return 0;
}

int array_remove(Array* arr, int i)
{
    if(i > arr->size || i < 0)
        return -1;
 
    for(int j = i; j < arr->size-1; j++) {
        arr->data[j] = arr->data[j+1];
    }

    arr->size--;

    return 0;
}
