#include <stdio.h>

typedef struct list list_node;

typedef struct list {
    int data;
    list_node *prev;
    list_node *next;
} list_node;

int main()
{
    
    list_node l1 = { 1, NULL, NULL};
    list_node l2 = { 2, NULL, NULL};
    list_node l3 = { 3, NULL, NULL};
    l1.next = &l2;
    l2.prev = &l1;
    l2.next = &l3;
    l3.prev = &l2;

    list_node *l = &l1;
    while(l!=NULL) {
        printf("%d\n", l->data);
        l = l->next;
    }

    return 0;
}

