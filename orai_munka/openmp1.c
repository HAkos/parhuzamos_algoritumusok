#include "openmp1.h"

int rand_range(int min, int max)
{
    return rand() % (max - min + 1) + min;
}

float calc_elapsed_ms(struct timespec *start, struct timespec *end)
{
    float elapsed_time;

    elapsed_time = (end->tv_sec - start->tv_sec)*1000;
    elapsed_time += (float) (end->tv_nsec - start->tv_nsec) / 1000000;

    return elapsed_time;
}

void print_array(Array arr)
{
    printf("Array size: %d\n", arr.size);
    for(int i = 0; i < arr.size; i++)
        printf("Array %d. element: %d\n", i, arr.data[i]);
}

void generate_array(Array arr, int min, int max)
{
    for(int i = 0; i < arr.size; i++) 
        arr.data[i] = rand_range(min, max);
}

void calc_prefix(Array arr)
{
    #pragma omp for
    for (int i = 0; i < arr.size; i++) {
        int sum = 0;
        for (int j = i; j < arr.size; j++) {
            sum += arr.data[i];
        }
        arr.data[i] = sum;
    }
}

void split_array(Array arr1, Array *arr2, Array *arr3)
{

    arr2->size = arr1.size / 2;
    arr3->size = arr1.size / 2;
    arr2->size = (arr1.size / 2) + arr1.size % 2;

    arr2->data = malloc(arr2->size*sizeof(int));
    arr3->data = malloc(arr3->size*sizeof(int));

    memcpy(arr2->data, arr1.data, arr2->size*sizeof(int));
    memcpy(arr3->data, &arr1.data[arr2->size], 
           arr3->size*sizeof(int));
}

void crew_prefix(Array sarr, Array *arr1, Array *arr2)
{
    split_array(sarr, arr1, arr2);
    calc_prefix(*arr1);
    calc_prefix(*arr2);
    for (int i = 0; i < arr2->size; i++)
    {
        arr2->data[i] += arr1->data[arr1->size-1];
    }

}

int main()
{
    srand(time(NULL));

    struct timespec start;
    struct timespec end;

    Array sarr;
    sarr.size = rand_range(10000,100000);
    int arr[sarr.size];
    sarr.data = arr;
    generate_array(sarr, 20, 100);

    //puts("Starting array:");
    //print_array(sarr);

    if (sarr.size == 1) {
        puts("Initial array size was 1, exiting.");
        exit(1); 
    }

    Array *arr1 = malloc(sizeof(int));
    Array *arr2 = malloc(sizeof(int));

    clock_gettime(CLOCK_MONOTONIC_RAW, &start);
    printf("start time: %d\n", start.tv_nsec);

    crew_prefix(sarr, arr1, arr2);
    
    clock_gettime(CLOCK_MONOTONIC_RAW, &end);
    printf("end time: %d\n", end.tv_nsec);
    printf("elapsed time: %f ms\n", calc_elapsed_ms(&start, &end));

    //puts("Array 1:");
    //print_array(*arr1);

    //puts("Array 2:");
    //print_array(*arr2);

    free(arr1);
    free(arr2);

    return 0;
}
