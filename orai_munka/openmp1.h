#include <omp.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>

typedef struct {
    size_t size;
    int* data;
} Array;

int rand_range(int min, int max);
float calc_elapsed_ms(struct timespec *start,
                      struct timespec *end);
void print_array(Array arr);
void generate_array(Array arr, int min, int max);
void calc_prefix(Array arr);
void split_array(Array arr1, Array *arr2, Array *arr3);
void crew_prefix(Array sarr, Array *arr1, Array *arr2);
