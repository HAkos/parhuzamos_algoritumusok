import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
lines = []

with open("time", "r") as f:
	lines = f.read().splitlines()

print(lines)

x = [l.split(',')[0] for l in lines]
y = [l.split(',')[1] for l in lines]

print(x)
print(y)

plt.scatter(x, y)

plt.xticks(rotation = 35)
plt.xlabel('Vizsgált intervallum (1..x)')
plt.ylabel('Futási idő')
plt.yticks([])

plt.tight_layout()
plt.savefig('runtime.png')
plt.show()
