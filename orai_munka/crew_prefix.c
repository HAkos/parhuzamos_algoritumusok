#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

typedef struct arr
{
    int* array;
    int size;
} arr_t;

int rand_range(int min, int max)
{
        return rand() % (max - min + 1) + min;
}

float calc_elapsed_ms(struct timespec *start, struct timespec *end)
{
    return (float) (end->tv_nsec - start->tv_nsec) / 1000000;
}

void print_array(arr_t arr)
{
    // printf("%x\n", arr.array);
    printf("Array size: %d\n", arr.size);
    for(int i =0; i < arr.size; i++)
        printf("%d\n", arr.array[i]);
}

void generate_array(arr_t arr)
{
    for(int i = 0; i < arr.size; i++) {
        arr.array[i] = rand_range(20, 100);
    }
}

void calc_prefix(arr_t arr1)
{
    // Nem tudom, hogy jol ertelmeztem-e az algoritmust

    for (int i = 0; i < arr1.size; i++) {
        int sum = 0;
        for (int j = i; j < arr1.size; j++) {
            sum += arr1.array[i];
        }
        arr1.array[i] = sum;
    }
}

void split_array(arr_t arr1, arr_t *arr2, arr_t *arr3)
{

    arr2->size = arr1.size / 2;
    arr3->size = arr1.size / 2;
    arr2->size = (arr1.size / 2) + arr1.size % 2;

    arr2->array = malloc(arr2->size*sizeof(int));
    arr3->array = malloc(arr3->size*sizeof(int));

    memcpy(arr2->array, arr1.array, arr2->size*sizeof(int));
    memcpy(arr3->array, &arr1.array[arr2->size], 
           arr3->size*sizeof(int));
}

void crew_prefix(arr_t sarr, arr_t *arr1, arr_t *arr2)
{
    split_array(sarr, arr1, arr2);
    calc_prefix(*arr1);
    calc_prefix(*arr2);
    for (int i = 0; i < arr2->size; i++)
    {
        arr2->array[i] += arr1->array[arr1->size-1];
    }

}

int main()
{
	srand(time(NULL));
    struct timespec start;
    struct timespec end;

    arr_t sarr;
    sarr.size = rand_range(1000,10000);
    int arr[sarr.size];
    sarr.array = arr;
    generate_array(sarr);

    //puts("Starting array:");
    //print_array(sarr);

    if (sarr.size == 1) {
        puts("Initial array size was 1, exiting.");
        exit(1); 
    }

    arr_t *arr1 = malloc(sizeof(int));
    arr_t *arr2 = malloc(sizeof(int));

    clock_gettime(CLOCK_MONOTONIC_RAW, &start);
    crew_prefix(sarr, arr1, arr2);
    clock_gettime(CLOCK_MONOTONIC_RAW, &end);
    printf("elapsed time: %f ms\n", calc_elapsed_ms(&start, &end));

    //puts("Array 1:");
    //print_array(*arr1);

    //puts("Array 2:");
    //print_array(*arr2);

    free(arr1);
    free(arr2);
}
