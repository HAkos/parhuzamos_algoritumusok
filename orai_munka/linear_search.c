#include <stdio.h>
#include <stdlib.h>
#include <time.h>



int rand_range(int min, int max)
{
	return rand() % (max - min + 1) + min;
}

void generate_array(int* arr, int arsz)
{
	printf("gen_arr, %d\n", arsz);
	arr = (int*) malloc(arsz*sizeof(int));
	if(arr == NULL)
	{
		perror("malloc error\n");
		exit(1);
	}


}

void free_array(int* arr)
{
	free(arr);
}

int get_random_element(int* arr, int arsz)
{
	int i = rand_range(0, arsz-1);
	return arr[i];
}

float calc_elapsed_ms(struct timespec *start, struct timespec *end)
{
  return (float) (end->tv_nsec - start->tv_nsec) / 1000000;
}

int main()
{
	srand(time(NULL));
	struct timespec start;
	struct timespec end;

	FILE* fp = fopen("time", "w");

	for (int i = 1000; i <= 30000; i+=1000) {
		int index = 0;
		int array[i];
		for (int k = 0; k < i; k++) {
			array[k] = rand_range(0,1000);
			//printf("%d,%d\n", arr[i], i);
		}
		int val = get_random_element(array, i);

		clock_gettime(CLOCK_MONOTONIC_RAW, &start);
		for(int k = 0; k < 10; k++) {
			for (int j = 0; j < i; j++) {
				if(array[j] == val) {
					index = j;
					clock_gettime(CLOCK_MONOTONIC_RAW, &end);
					float elapsed = calc_elapsed_ms(&start, &end);
					fprintf(fp, "%d,%f\n", i, elapsed);
					printf("%d,%f\n", i, elapsed);
					break;
				}
			}
			//free_array(array);
		}

	}
	fclose(fp);
	return 0;
}
