#include <pthread.h>
#include <time.h>
#include <stdbool.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

#define THREADC 20
static int* primec;

bool is_prime(int n) {

    if (n <= 1)
        return false;
    for (int i = 2; i < sqrt(n); i++) {
        if (n % i == 0) {
            return false;
            break;
        } 
    }
    return true;
}

float calc_elapsed_ms(struct timespec *start, struct timespec *end)
{
  return (float) (end->tv_nsec - start->tv_nsec) / 1000000;
}

void* thread_task(void* parg)
{
    int eindex = parg; 
    int primecount = 0;
    //printf("startindex %d\n", *sindex);
    printf("end index: %d\n", eindex); 

    for (int i = 1; i < eindex; i++) {
        if (is_prime(i))
            primecount++;
    }
    //printf("primecount: %d\n", primecount);
    pthread_exit((void*)primecount); 
}

int main()
{
    primec = (int*)malloc(THREADC*sizeof(int));

    pthread_t threads[THREADC];
    int ti = 0;
    int i;

    struct timespec start;
    struct timespec end;
    clock_gettime(CLOCK_MONOTONIC_RAW, &start);
    for (i = 1000; i <= 20000; i+=1000) {
        pthread_create(&threads[ti], NULL, thread_task, (void*)i);
        ti++;
    }
    for (ti = 0; ti < THREADC; ti++) {
        int *retval;
        pthread_join(threads[ti], (void**)&retval);
        //printf("primecount: %d\n", *retval); 
    }
    clock_gettime(CLOCK_MONOTONIC_RAW, &end);
    float time = calc_elapsed_ms(&start, &end);
    printf("elapsed time: %f ms\n", time);
}
