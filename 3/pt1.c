#include <pthread.h>
#include <time.h>
#include <stdbool.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

#define THREADC 60

void* pthread_func(void* parg);

int main()
{
	pthread_t p[THREADC];
	for (int i = 0; i < THREADC; i++) {
        char *str = malloc(30);
        sprintf(str, "Thread %d", i);
		pthread_create(&p[i], NULL, pthread_func, (void*)str);
	}
    
    //sleep(2);    
    
	for (int i = 0; i < THREADC; i++) {
		char* retval;
		pthread_join(p[i], (void**)&retval);
		puts(retval);
	}

	return 0;
}

void* pthread_func(void *parg)
{
	time_t start;
	time_t end;

	time(&start);

	do {
		for (int i = 0; i < 100; i++) {
			double f = sqrt(i*(i/1.234));
			double f1 = (f*123)/34.1234;
		}
		time(&end);
	} while (end - start < 1);
	pthread_exit(parg);
    free(parg);
}
