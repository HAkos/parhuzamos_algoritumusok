/*
 *  https://en.wikipedia.org/wiki/Machin-like_formula
 */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <mpfr.h>
#include <omp.h>

float calc_elapsed_sec(struct timespec *start, struct timespec *end)
{
    float elapsed_time;

    elapsed_time = (end->tv_sec - start->tv_sec);
    elapsed_time += (float) (end->tv_nsec - start->tv_nsec) / 1000000000;

    return elapsed_time;
}

size_t check_correct_digits(mpfr_t *pi)
{
    char c;
    size_t correct_digits = 0;
    FILE* fp = fopen("1m_pi", "r");
    if (fp == NULL) {
        puts("couldnt open file");
        exit(1);
    }

    char *pi_string = malloc(mpfr_get_prec(*pi) + 100);
    mpfr_sprintf(pi_string, "%.*RNf\n", mpfr_get_prec(*pi), *pi);

    while ((c = fgetc(fp)) != EOF) {
        if (c == pi_string[correct_digits])
            correct_digits++;
        else
            break;
    }

    return correct_digits;
}

int main(int argc, char **argv)
{
    if (argc == 1) {
        printf("Usage: %s [precision in bits]\n", argv[0]);
        puts("Number of digits = precision/3.35 (approximately)");
        exit(1);
    }

    int quiet = 0;
    size_t prec = 0;
    prec = atoi(argv[1]);
    if (prec == 0)
        prec = 5000;
    
    if (argc > 2 && strcmp("-q", argv[2]) == 0)
        quiet = 1;

    struct timespec start, end;
    mpfr_t pi, a1, a2, a3, a4, one;
    mpfr_t b1, b2, b3, b4;

    clock_gettime(CLOCK_MONOTONIC_RAW, &start);

    mpfr_init2(pi, prec);
    mpfr_init2(a1, prec);
    mpfr_init2(a2, prec);
    mpfr_init2(a3, prec);
    mpfr_init2(a4, prec);
    mpfr_init2(b1, prec);
    mpfr_init2(b2, prec);
    mpfr_init2(b3, prec);
    mpfr_init2(b4, prec);
    mpfr_init2(one, 100);

    mpfr_set_ui(one, 1, MPFR_RNDN);
    mpfr_set_ui(pi, 0, MPFR_RNDN);

    mpfr_set_ui(b1, 49, MPFR_RNDN);
    mpfr_set_ui(b2, 57, MPFR_RNDN);
    mpfr_set_ui(b3, 239, MPFR_RNDN);
    mpfr_set_ui(b4, 110443, MPFR_RNDN);

    #pragma omp parallel sections
    {
        #pragma omp section
        {
            mpfr_atan2(a1, one, b1, MPFR_RNDN);
            mpfr_mul_ui(a1, a1, 12, MPFR_RNDN);
        }

        #pragma omp section
        {
            mpfr_atan2(a2, one, b2, MPFR_RNDN);
            mpfr_mul_ui(a2, a2, 32, MPFR_RNDN);
        }

        #pragma omp section
        {
            mpfr_atan2(a3, one, b3, MPFR_RNDN);
            mpfr_mul_ui(a3, a3, 5, MPFR_RNDN);
        }

        #pragma omp section
        {
            mpfr_atan2(a4, one, b4, MPFR_RNDN);
            mpfr_mul_ui(a4, a4, 12, MPFR_RNDN);
        }
    }

    mpfr_add(pi, pi, a1, MPFR_RNDN); 
    mpfr_add(pi, pi, a2, MPFR_RNDN); 
    mpfr_sub(pi, pi, a3, MPFR_RNDN); 
    mpfr_add(pi, pi, a4, MPFR_RNDN);

    mpfr_mul_ui(pi, pi, 4, MPFR_RNDN);

    clock_gettime(CLOCK_MONOTONIC_RAW, &end);
    printf("%f\n", calc_elapsed_sec(&start, &end));
    if (!quiet) {
        mpfr_printf("%.*RNf\n", mpfr_get_prec(pi), pi);
        printf("correct digits: %ld\n", check_correct_digits(&pi));
    }
    return 0;
}
