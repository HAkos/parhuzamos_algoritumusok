import subprocess
import matplotlib.pyplot as plt
import numpy

seq = []
parallel = []

prec=100000

for x in range(1, 21):
    y = x*prec
    seq.append(tuple((y, float(subprocess.run(['./seq_pi', f'{y}', '-q'], capture_output=True, encoding='utf8').stdout))))
    print('Seq: ' + str(seq[x-1]))

for x in range(1, 21):
    y = x*prec
    parallel.append(tuple((y, float(subprocess.run(['./parallel_pi', f'{y}', '-q'], capture_output=True, encoding='utf8').stdout))))
    print('Parallel: ' + str(parallel[x-1]))

x1, y1 = zip(*seq)
x2, y2 = zip(*parallel)

x = numpy.arange(len(x1))
w = 0.25

plt.bar(x, y1, color='b', width=w, label='Sequential')
plt.bar(x + w, y2, color='r', width=w, label='Parallel')

plt.xlabel(f'Precision (*{prec})')
plt.ylabel('Time in seconds')

plt.xticks(x+w/2, [int(xt/prec) for xt in x1])
plt.legend()

plt.savefig('graph.png')
plt.show()
