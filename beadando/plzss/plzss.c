#include "plzss.h"

size_t get_file_len(int fd)
{
    struct stat s;
    fstat(fd, &s);
    return s.st_size;
}

void init_tasks(size_t filelen, Task* tasks, uint32_t threadc, int ifd, FILE* ofp)
{
    size_t tasksize = floor(filelen/threadc);
    tasks[0].start = 0;
    tasks[0].end = tasksize;
    for (int i = 1; i < threadc; i++) {
        tasks[i].start = tasks[i-1].end;
        tasks[i].end = tasks[i].start + tasksize - 1;
    }
    
    for (int i = 0; i < threadc; i++) {
        printf("task start: %d\n", tasks[i].start);
        printf("task end: %d\n", tasks[i].end);

        sprintf(tasks[i].tofname, "%d.part", i);
        tasks[i].tofile = fopen(tasks[i].tofname, "w+");

        if (tasks[i].tofile == NULL) {
            puts("error opening output file!");
            exit(1);
        }

        tasks[i].swbstart = tasks[i].start;
        tasks[i].swbend = tasks[i].start;
        //if (BUFSIZE > tasksize)
        //   tasks[i].swbend = tasks[i].end;
        //else
        //   tasks[i].swbend = tasks[i].swbstart + BUFSIZE;
        tasks[i].ifile = ifd;
        tasks[i].ofile = ofp;

        printf("task swbstart: %d\n", tasks[i].swbstart);
        printf("task swbend: %d\n", tasks[i].swbend);
    }
    
    tasks[threadc-1].end = filelen;
}

void start_tasks(size_t filelen, Task* tasks, uint32_t threadc)
{
    int fd = tasks[0].ifile;
    buffer = mmap(NULL, filelen, PROT_READ|PROT_WRITE, MAP_PRIVATE, fd, 0);
    if (buffer == MAP_FAILED) {
        puts("mmap failed");
        exit(1);
    }

    for (int i = 0; i < threadc; i++) {
        pthread_create(&tasks[i].thread, NULL, thread_job_encode, (void*)&tasks[i]); 
    }
}

void end_tasks(Task *tasks, uint32_t threadc)
{
    for (int i = 0; i < threadc; i++) {
        pthread_join(tasks[i].thread, NULL);
    }
    merge_files(tasks, threadc);
}

void* thread_job_encode(void* parg)
{
    Task *t = (Task*)parg;
    
    int j;
    int i = t->start;
    
    while (i < t->end) {
        if (t->swbend < t->end) 
            t->swbend++;
        if ((t->swbend - t->swbstart) >= BUFSIZE) {
            t->swbstart++;
        }

        Token tk;
        tk = find_match(t, i);
        char buf[100];
        snprintf(buf, 100, "<%ld,%d>", tk.position, tk.length);
        if (tk.position != 0 && tk.length != 0 && 
            strlen(buf) < tk.length && tk.position < i-tk.length) {
            // printf("found match at: %d length: %d\n", tk.position, tk.length);
            /*
            printf("%.*s (%d, %d)\nmatching\n%.*s (%d, %d)\n",
                    tk.length, &buffer[i], i,
                    tk.length, tk.length, &buffer[tk.position],
                    tk.position, tk.length);
            */
            fputs(buf, t->tofile);
            i += tk.length;
        } else {
            fputc(buffer[i], t->tofile);
            i++;
        }
    }

    fclose(t->tofile);
}

void decode(int ifd, FILE* ofp, size_t filelen)
{
    buffer = mmap(NULL, filelen, PROT_READ|PROT_WRITE, MAP_PRIVATE, ifd, 0);
    if (buffer == MAP_FAILED) {
        puts("mmap failed");
        exit(1);
    }
    int j, length, pos, poslen;
    int i = 0;
    char poss[50];
    char full_str[100];
    
    while (i < filelen) {
        if (buffer[i] == '<' && i < filelen-1 && isdigit(buffer[i+1])) {
            pos = strtol(&buffer[i+1], NULL, 10);
            memset(poss, 0, 50);
            snprintf(poss, 50, "%d", pos);
            poslen = strlen(poss);

            length = strtol(&buffer[i+poslen+2], NULL, 10);
            memset(full_str, 0, 100);
            snprintf(full_str, 100, "<%d,%d>", pos, length);
            //printf("pos %d, length: %d\n", pos, length);

            char str[length];

            if (fseek(ofp, pos, SEEK_SET) != 0)
                puts("fseek failed");

            for (j = 0; j < length; j++)
                str[j] = fgetc(ofp);

            if (fseek(ofp, 0, SEEK_END) != 0)
                puts("fseek failed");
            
            for (j = 0; j < length; j++)
                fputc(str[j], ofp);

            i += strlen(full_str);
        } else {
            fputc(buffer[i], ofp);
            fflush(ofp);
            i++;
        }
    }

    fclose(ofp);
}

Token find_match(Task *t, size_t cur_pos)
{
    Token tk;
    tk.position = 0;
    tk.length = 0;

    size_t buflen = t->end; 

    size_t lookahead = cur_pos + LOOKAHEAD_BUF_SIZE;
    size_t max_len = lookahead > buflen ? buflen : lookahead;
    
    int substring_sz = 0;
    int i, j;

    // printf("cur_pos: %d, max_len: %d, swbstart: %d\n", cur_pos, max_len, t->swbstart);
    for (i = cur_pos; i < max_len; i++) {
        substring_sz = i - cur_pos;
        // printf("substring sz: %d\n", substring_sz);
        // char str[substring_sz];
        // strncpy(str, &buffer[cur_pos], substring_sz);
        if (substring_sz != 0) {
            for (j = t->swbstart; j < t->swbend; j++) {
                if (j > i)
                    continue;
                if (j + substring_sz < i) {
                    //printf("i: %d j: %d\n", i, j);
                    // printf("substring size: %d\n", substring_sz);
                    // char str1[substring_sz];
                    // strncpy(str1, &(t->swb[j]), substring_sz);
                    // printf("%.*s\n", substring_sz, &(buffer[cur_pos]));
                    
                    if (strncmp(&(buffer[cur_pos]), &(buffer[j]), substring_sz) == 0) {
                        //printf("match at pos %d len %d\n", j, substring_sz);
                        if (substring_sz > tk.length) {
                            tk.position = j;
                            tk.length = substring_sz;
                        }
                    }
                }
            }
        }
    }
    
    //printf("best match substring sz: %d comparing %.*s to ", substring_sz, substring_sz, &(buffer[cur_pos]));
    //printf("%.*s\n", substring_sz, &(buffer[cur_pos]));
    return tk;
}

void merge_files(Task *tasks, uint32_t threadc)
{
    char c;
    for (int i = 0; i < threadc; i++) {
        tasks[i].tofile = fopen(tasks[i].tofname, "r");
        while ((c = fgetc(tasks[i].tofile)) != EOF) {
            fprintf(tasks[i].ofile, "%c", c);
            //printf("%c", c);
        }
        
        fclose(tasks[i].tofile);
        if (remove(tasks[i].tofname) != 0)
           printf("couldnt remove a temporary file! %s\n", strerror(errno));
    }
}

char* get_str(FILE* fp, long pos, int size)
{
    long orig_pos = ftell(fp);
    fseek(fp, pos, SEEK_SET);
}
