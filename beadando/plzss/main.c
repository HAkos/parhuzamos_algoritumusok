#include "plzss.h"

int main(int argc, char** argv)
{
    bool encode;
    if (argc < 2) {
        printf("Usage: %s [e/d] [INPUT] [OUTPUT] [[THREADCOUNT]]\n", argv[0]);
        exit(1);
    }

    if (strcmp(argv[1], "e") != 0 && strcmp(argv[1], "d") != 0) {
        printf("Usage: %s [e/d] [INPUT] [OUTPUT] [[THREADCOUNT]]\n", argv[0]);
        exit(1);
    }

    if(strcmp(argv[1], "d") == 0)
    {
        if (argc != 4) {
            printf("Usage: %s [e/d] [INPUT] [OUTPUT]\n", argv[0]);
            exit(1);
        }
    } else {
        if (argc != 5) {
            printf("Usage: %s [e/d] [INPUT] [OUTPUT] [THREADCOUNT]\n", argv[0]);
            exit(1);
        }
    }

    if (strcmp(argv[1], "e") == 0)
        encode = true;
    else
        encode = false;

    int ifile = open(argv[2], O_RDWR); 
    if (ifile < 0) {
        puts("opening input file failed");
        exit(1);
    }
    
    FILE* ofile = fopen(argv[3], "w+");
    if (ofile == NULL) {
        puts("opening output file failed");
        exit(1);
    }
    
    size_t file_len = get_file_len(ifile);

    if (encode) {
        unsigned int threadc = atoi(argv[4]);
        Task *tasks;
        tasks = malloc(threadc * sizeof(Task));

        init_tasks(file_len, tasks, threadc, ifile, ofile);
        start_tasks(file_len, tasks, threadc);
        end_tasks(tasks, threadc);

    } else {
        decode(ifile, ofile, file_len);
    }

    return 0;
}
