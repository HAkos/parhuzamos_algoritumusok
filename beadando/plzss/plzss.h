#ifndef PLZSS_H
#define PLZSS_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <pthread.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include <ctype.h>

#define BUFSIZE 4096
#define LOOKAHEAD_BUF_SIZE 20

static char* buffer;

typedef struct {
    size_t position;
    uint16_t length;
} Token;

typedef struct
{
    /* Input file slice start index */
    size_t start;
    
    /* Input file slice end index */
    size_t end;

    pthread_t thread;

    /* Sliding window buffer */ 
    // char* swb;

    /* Sliding window buffer start index */ 
    size_t swbstart;

    /* Sliding window buffer end index */ 
    size_t swbend;

    /* Input file file descriptor */
    int ifile;

    /* Output file file descriptor */
    FILE* ofile;

    /* Thread-specific output file */
    FILE* tofile;

    /* Thread-specific output file name */
    char tofname[15];
} Task;

size_t get_file_len(int fd);
void init_tasks(size_t filelen, Task* tasks, uint32_t threadc, int ifd, FILE* ofp);
void start_tasks(size_t filelen, Task* tasks, uint32_t threadc);
void end_tasks(Task *tasks, uint32_t threadc);
void* thread_job_encode(void* parg);
// void* thread_job_decode(void* parg);

void decode(int ifd, FILE* ofp, size_t filelen);

/* Adds a character to the sliding window 
 * of the thread passed as a parameter.
 * If the buffer is full, "slide" the window. */
// void add_to_swb(Task *t, char c);

/* Search for substrings in the sliding window matching the 
 * current position+LOOKAHEAD_BUF_SIZE substring */
Token find_match(Task *t, size_t cur_pos);

/* The threads create separate output files.
 * This function merges those output files into 
 * one output file after the encoding/decoding
 * has finished */
void merge_files(Task *tasks, uint32_t threadc);

char* get_str(FILE* fp, long pos, int size);

#endif
